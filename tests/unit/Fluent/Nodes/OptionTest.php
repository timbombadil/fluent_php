<?php
namespace Fluent;

use Fluent\Nodes\Option;
use PHPUnit\Framework\TestCase;

class OptionTest extends TestCase {

    public function testParser() {
        $string = <<<TEXT
    [null] test string
[one] another string
   *[3] more strings
TEXT;

        $firstOption = new Option($string, []);
        $secondOption = new Option($firstOption->getSubString(), []);
        $thirdOption = new Option($secondOption->getSubString(), []);

        $this->assertEquals(" test string", $firstOption->getValue());
        $this->assertEquals('null', $firstOption->getKey());
        $this->assertFalse($firstOption->isDefault());

        $this->assertEquals("
[one] another string
   *[3] more strings", $firstOption->getSubString());

        $this->assertEquals(" another string", $secondOption->getValue());
        $this->assertEquals('one', $secondOption->getKey());
        $this->assertFalse($secondOption->isDefault());

        $this->assertEquals(" more strings", $thirdOption->getValue());
        $this->assertEquals('3', $thirdOption->getKey());
        $this->assertTrue($thirdOption->isDefault());
    }
}
