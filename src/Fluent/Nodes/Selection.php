<?php
namespace Fluent\Nodes;

final class Selection {
    private $pattern = '/\{\s*\$([\S]+)\s*\-\>\s*(.*).\s*\}/s';

    private $key;

    private $value;

    private $substring;

    public function __construct(string $string, array $arguments) {
        preg_match($this->pattern, $string, $match, PREG_OFFSET_CAPTURE);

        $this->key = isset($arguments[$match[1][0]]) ? $arguments[$match[1][0]] : '*';
        $options = [];

        $value = $match[2][0];
        while(!empty($value)) {
            $option = new Option($value, $arguments);
            if ($option->getKey() === 'one') {
                $options[1] = $option;
            }
            if ($option->isDefault()) {
                $options['*'] = $option;
            }
            $options[$option->getKey()] = $option;

            $value = $option->getSubString();
        }

        $offset = strlen($match[0][0]) + $match[0][1];
        $this->substring = substr($string, $offset);

        if (isset($options[$this->key])) {
            $this->value = substr($string, 0, $match[0][1]) . $options[$this->key]->getValue();
        } else {
            $this->value = substr($string, 0, $match[0][1]) . $options['other']->getValue();
        }
    }

    public function getKey() {
        return $this->key;
    }

    public function getValue() {
        return $this->value;
    }

    public function getSubString() {
        return $this->substring;
    }
}