<?php

namespace Fluent\Nodes;


final class Variable {
    private $pattern = '/\{\s*\$([^\s]+)\s*\}/';

    private $value;

    public function __construct(string $string, array $arguments) {
            $this-> value = preg_replace_callback($this->pattern, function($match) use ($arguments) {
                $variable = $match[1];

                if (!isset($arguments[$variable])) {
                    $variables = implode(', ', array_keys($arguments));

                    throw new \Exception("No variable defined for '{$variable}', avaliable variables are [{$variables}]");
                }

                return $arguments[$variable];
            }, $string);
    }

    public function __toString() {
        return $this->value;
    }
}