<?php

namespace Fluent\Nodes;


final class Option {
    private $pattern = '/(((\*?)\[(\w+)\])(.+))([\s]+(?1)|$)/sU';

    /** @var bool bool */
    private $default;

    /** @var string */
    private $key;

    /** @var string */
    private $value;

    private $substring;

    public function __construct(string $string, array $arguments) {
        preg_match($this->pattern, $string, $match, PREG_OFFSET_CAPTURE);

        $this->default = $match[3][0] === '*';
        $this->key = (string)$match[4][0];
        $this->value = substr($string, 0, $match[1][1]) . (string)$match[5][0];

        $offset = strlen($match[1][0]) + $match[1][1];
        $this->substring = substr($string, $offset);
    }

    public function isDefault() {
        return $this->default;
    }

    public function getKey() {
        return $this->key;
    }

    public function getValue() {
        return $this->value;
    }

    public function getSubString() {
        return $this->substring;
    }
}