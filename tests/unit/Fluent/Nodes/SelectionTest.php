<?php

namespace Fluent\Nodes;

use PHPUnit\Framework\TestCase;

class SelectionTest extends TestCase {
    public function testParser() {
        $string = <<<TEXT
I give {\$fucks ->
  [0] zero fucks
  [1] a single fuck
  *[other] {\$fucks} fucks
  }
TEXT;


        $testee = new Selection($string, ['fucks' => 2]);

        $this->assertEquals('fucks', $testee->getKey());
        $this->assertEquals('I give 
   {$fucks} fucks
 ', $testee->getValue());
    }
}
