<?php
namespace Fluent;

use Fluent\Nodes\Selection;
use Fluent\Nodes\Variable;

final class FluentParser {

    public function parse(string $string, array $arguments) {
        $selection = new Selection($string, $arguments);

        $value = $selection->getValue() . $selection->getSubString();
        $variables = new Variable($value, $arguments);

        echo (string)$variables;
    }

}